This team - [atlaskit-collaborators](https://bitbucket.org/atlaskit-collaborators/) contains developers who contribute code to [Atlaskit-MK-2](https://bitbucket.org/atlassian/atlaskit-mk-2). For information on how to use Atlaskit components in your projects, please head to http://atlaskit.atlassian.com and http://atlassian.design .

To become a collaborator, please raise an issue at https://bitbucket.org/atlassian/atlaskit-mk-2/issues?status=new&status=open with the following.

1. Issue title - **"Become an Atlaskit collaborator"**
2. Bitbucket ID and
3. Reference another issue of which you'd like to provide a PR or any other reasons.
4. Be ready to sign the CLA. [Read the "Contributors"](https://bitbucket.org/atlassian/atlaskit-mk-2/src/master/) section of the Atlaskit README.

Once processed, you will have access to create branches in Atlaskit-MK-2 where builds are created by our Bitbucket Pipelines.

---

